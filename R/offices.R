#' Office locations
#'
#' a supplemental data set for Chrunchbase2013 with information on
#' objects (companies or Financial organisations) office locations
#'
#' @format A data frame with 112718 rows and 15 variables:
#' \describe{
#'   \item{id}{row number that can be used for speedy data extraction}
#'   \item{object_id}{an id for an aorganization from *object* database}
#'   \item{office_id}{unique office id}
#'   \item{description}{Office description, eg.Lala Headquarters, HQ etc}
#'   \item{region}{Region location}
#'   \item{address1}{The First address line}
#'   \item{address2}{The second address line}
#'   \item{city}{}
#'   \item{zip_code}
#'   \item{state_code}{}
#'   \item{countrly_code}{}
#'   \item{latitude}{}
#'   \item{longitude}{}
#'   \item{created_at}{record creation date}
#'   \item{updated_at}{last update date}
#'   ...
#' }
#' @source \url{https://www.kaggle.com/justinas/startup-investments, https://www.crunchbase.com/}
"offices"
