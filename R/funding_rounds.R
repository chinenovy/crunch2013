#' Funding_rounds
#'
#' a supplemental data set for Chrunchbase2013 with information on
#' type and the amount of funding, and company pre- and post funding valuation.
#'
#' @format A data frame with 52928 rows and 23 variables:
#' \describe{
#'   \item{id}{row number that can be used for speedy data extraction}
#'   \item{funding_round_id}{unique funding event id}
#'   \item{object_id}{an id for a company from *object* data database, always starts with c:}
#'   \item{funded_at}{date of funding}
#'   \item{funding_round_type}{Type of funding: angel,series-a,series-b,series-c+,
#'   crowd funding, post-ipo, private-equity, venture or other}
#'   \item{funding_round_code}{Not imidiately clear, partially overallpin with funding_round_type}
#'   \item{raised_amount_usd}{Raised amount in the US dollars}
#'   \item{raised_amount}{Raised amount in the original curency}
#'   \item{raised_currency_code}{Currency code for the original curency in raised_amount column}
#'   \item{pre_money_valuation_usd}{Company valuation before funding round in the US dollars}
#'   \item{pre_money_valuation}{Company valuation before funding round, original curency}
#'   \item{post_money_valuation_usd}{Company valuation following the funding round in the US dollars}
#'   \item{post_money_valuation}{Company valuation following the funding round in the original curency}
#'   \item{post_money_currency_code}{Currency code for the original curency}
#'   \item{participants}{The number of entities involved in funding}
#'   \item{is_first_round}{1 for the first round of funding, 0 otherwise}
#'   \item{is_last_round}{1 for the last round of funding, 0 otherwise}
#'   \item{source_url}{Sourse of information}
#'   \item{source_description}{Some description of the source, article title or source name}
#'   \item{created_by}{id of record's creator}
#'   \item{created_at}{record creation date}
#'   \item{updated_at}{last update date}
#'   ...
#' }
#' @source \url{https://www.kaggle.com/justinas/startup-investments, https://www.crunchbase.com/}
"funding_rounds"
